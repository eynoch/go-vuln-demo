module github.com/rudecs/terraform-provider-decort

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/google/uuid v1.3.0
	github.com/hashicorp/terraform-plugin-docs v0.5.1
	github.com/hashicorp/terraform-plugin-sdk v1.16.0
	github.com/sirupsen/logrus v1.7.0
)
