### Bug fixes
- resgroup recreation if quotas unspecified

### New datasources
- vgpu
- pcidevice\_list
- pcidevice
- sep
- sep\_list
- sep\_disk\_list
- sep\_config
- sep\_pool
- sep\_consumption

### New resources
- pcidevice
- sep
- sep\_config
